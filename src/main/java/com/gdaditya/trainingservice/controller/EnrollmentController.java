package com.gdaditya.trainingservice.controller;

import com.gdaditya.trainingservice.exception.BaseException;
import com.gdaditya.trainingservice.exception.JwtTokenMalformedException;
import com.gdaditya.trainingservice.exception.JwtTokenMissingException;
import com.gdaditya.trainingservice.exception.UnauthorizedException;
import com.gdaditya.trainingservice.model.dto.BaseResponse;
import com.gdaditya.trainingservice.model.dto.EnrollmentCreateInput;
import com.gdaditya.trainingservice.model.dto.EnrollmentUpdateInput;
import com.gdaditya.trainingservice.service.EnrollmentService;
import com.gdaditya.trainingservice.util.JwtUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Log4j2
@RestController
@RequestMapping("/api/enrollment")
@RequiredArgsConstructor
public class EnrollmentController {
    private final EnrollmentService enrollmentService;
    private final JwtUtil jwtUtil;

    @PostMapping("/")
    public ResponseEntity<?> createEnrollment(
            @RequestBody @Valid EnrollmentCreateInput req,
            @RequestHeader("Authorization") String token
    ) throws JwtTokenMalformedException, JwtTokenMissingException, BaseException {
        if (isTokenNotRole(token, "LEARNER"))
            throw new UnauthorizedException();
        return ResponseEntity.ok(new BaseResponse<>(enrollmentService.createEnrollment(req, token)));
    }

    @GetMapping("/")
    public ResponseEntity<?> getEnrolledTrainings(
            @RequestHeader("Authorization") String token
    ) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        Long userId = jwtUtil.getUserId(token);
        return ResponseEntity.ok(new BaseResponse<>(enrollmentService.getEnrollmentFromUserId(userId, token)));
    }

    @GetMapping("/{userId}/{trainingId}")
    public ResponseEntity<?> getEnrollment(
            @PathVariable("userId") Long userId,
            @PathVariable("trainingId") Long trainingId,
            @RequestHeader("Authorization") String token
    ) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        return ResponseEntity.ok(new BaseResponse<>(enrollmentService.getEnrollment(userId, trainingId, token)));
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<?> getEnrollmentFromUserId(
            @PathVariable("id") Long userId,
            @RequestHeader("Authorization") String token
    ) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        return ResponseEntity.ok(new BaseResponse<>(enrollmentService.getEnrollmentFromUserId(userId, token)));
    }

    @GetMapping("/training/{id}")
    public ResponseEntity<?> getEnrollmentFromTrainingId(
            @PathVariable("id") Long trainingId,
            @RequestHeader("Authorization") String token
    ) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        return ResponseEntity.ok(new BaseResponse<>(enrollmentService.getEnrollmentFromTrainingId(trainingId, token)));
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAllEnrollment(@RequestHeader("Authorization") String token) throws JwtTokenMalformedException, JwtTokenMissingException, UnauthorizedException {
        if (isTokenNotRole(token, "ADMIN"))
            throw new UnauthorizedException();
        return ResponseEntity.ok(new BaseResponse<>(enrollmentService.getAllEnrollment()));
    }

    @PatchMapping("/")
    public ResponseEntity<?> updateEnrollment(
            @RequestBody @Valid EnrollmentUpdateInput req,
            @RequestHeader("Authorization") String token
    ) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        if (isTokenNotRole(token, "ADMIN"))
            throw new UnauthorizedException();
        return ResponseEntity.ok(new BaseResponse<>(enrollmentService.updateEnrollment(req)));
    }

    @DeleteMapping("/{userId}/{trainingId}")
    public ResponseEntity<?> deleteEnrollment(
            @PathVariable("userId") Long userId,
            @PathVariable("trainingId") Long trainingId,
            @RequestHeader("Authorization") String token
    ) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        if (isTokenNotRole(token, "ADMIN"))
            throw new UnauthorizedException();
        enrollmentService.deleteEnrollment(userId, trainingId);
        return ResponseEntity.ok(new BaseResponse<>(null));
    }

    private boolean isTokenNotRole(String token, String role) throws JwtTokenMalformedException, JwtTokenMissingException {
        return !jwtUtil.getUserRole(token).equals(role);
    }
}
