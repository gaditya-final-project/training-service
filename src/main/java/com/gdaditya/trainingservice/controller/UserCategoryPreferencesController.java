package com.gdaditya.trainingservice.controller;

import com.gdaditya.trainingservice.exception.BaseException;
import com.gdaditya.trainingservice.exception.JwtTokenMalformedException;
import com.gdaditya.trainingservice.exception.JwtTokenMissingException;
import com.gdaditya.trainingservice.model.dto.BaseResponse;
import com.gdaditya.trainingservice.service.UserCategoryPreferencesService;
import com.gdaditya.trainingservice.util.JwtUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/preferences")
@RequiredArgsConstructor
public class UserCategoryPreferencesController {

    private final UserCategoryPreferencesService userCategoryPreferencesService;
    private final JwtUtil jwtUtil;

    @PostMapping("/{categoryId}")
    public ResponseEntity<?> createPreferences(
            @PathVariable("categoryId") Long categoryId,
            @RequestHeader("Authorization") String token
    ) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        Long userId = jwtUtil.getUserId(token);
        return ResponseEntity.ok(new BaseResponse<>(
                userCategoryPreferencesService.createUserCategoryPreferences(userId, categoryId)));
    }

    @GetMapping("/{userId}")
    public ResponseEntity<?> getPreferencesFromUserId(@PathVariable("userId") Long userId) throws BaseException {
        return ResponseEntity.ok(new BaseResponse<>(userCategoryPreferencesService.getAllUserCategoryPreferencesFromUserId(userId)));
    }

    @GetMapping("/category/{categoryId}")
    public ResponseEntity<?> getPreferencesFromCategoryId(@PathVariable("categoryId") Long categoryId) {
        return ResponseEntity.ok(new BaseResponse<>(userCategoryPreferencesService.getAllUserCategoryPreferencesFromCategoryId(categoryId)));
    }

    @DeleteMapping("/{categoryId}")
    public ResponseEntity<?> deletePreferences(
            @PathVariable("categoryId") Long categoryId,
            @RequestHeader("Authorization") String token
    ) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        Long userId = jwtUtil.getUserId(token);
        userCategoryPreferencesService.deleteUserCategoryPreferences(userId, categoryId);
        return ResponseEntity.ok(new BaseResponse<>());
    }

}
