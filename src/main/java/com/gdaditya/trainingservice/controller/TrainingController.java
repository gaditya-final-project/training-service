package com.gdaditya.trainingservice.controller;

import com.gdaditya.trainingservice.exception.BaseException;
import com.gdaditya.trainingservice.exception.JwtTokenMalformedException;
import com.gdaditya.trainingservice.exception.JwtTokenMissingException;
import com.gdaditya.trainingservice.exception.UnauthorizedException;
import com.gdaditya.trainingservice.model.dto.BaseResponse;
import com.gdaditya.trainingservice.model.dto.TrainingCreateInput;
import com.gdaditya.trainingservice.model.dto.TrainingUpdateInput;
import com.gdaditya.trainingservice.service.TrainingService;
import com.gdaditya.trainingservice.service.UserService;
import com.gdaditya.trainingservice.util.JwtUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.ws.rs.Path;

@Log4j2
@RestController
@RequestMapping("/api/training")
@RequiredArgsConstructor
public class TrainingController {

    private final TrainingService trainingService;
    private final JwtUtil jwtUtil;

    @PostMapping("/")
    public ResponseEntity<?> createTraining(
            @RequestBody @Valid TrainingCreateInput trainingCreateInput,
            @RequestHeader("Authorization") String token
            ) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        if (isTokenNotRole(token,"TRAINER"))
            throw new UnauthorizedException();
        return ResponseEntity.ok(new BaseResponse<>(trainingService.createTraining(trainingCreateInput, token)));
    }

    @GetMapping("/")
    public ResponseEntity<?> getCreatedTraining(
            @RequestHeader("Authorization") String token
    ) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        return ResponseEntity.ok(new BaseResponse<>(trainingService.getAllCreatedTraining(token)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getTraining(
            @PathVariable("id") Long id
    ) throws BaseException {
        return ResponseEntity.ok(new BaseResponse<>(trainingService.getTrainingFromId(id)));
    }

    @GetMapping("/category/{id}")
    public ResponseEntity<?> getCreatedTrainingFromCategory(
            @PathVariable("id") Long id
    ) throws BaseException {
        return ResponseEntity.ok(new BaseResponse<>(trainingService.getAllTrainingFromCategory(id)));
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<?> getCreatedTrainingFromUser(
            @PathVariable("id") Long id
    ) throws BaseException {
        return ResponseEntity.ok(new BaseResponse<>(trainingService.getAllTrainingFromTrainer(id)));
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAllTraining() throws BaseException {
        return ResponseEntity.ok(new BaseResponse<>(trainingService.getAllTraining()));
    }

    @PatchMapping("/")
    public ResponseEntity<?> updateTraining(
            @RequestBody @Valid TrainingUpdateInput req,
            @RequestHeader("Authorization") String token
    ) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        return ResponseEntity.ok(new BaseResponse<>(trainingService.updateTraining(req, token)));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteTraining(@PathVariable("id") Long id) throws BaseException {
        trainingService.deleteTrainingFromId(id);
        return ResponseEntity.ok(new BaseResponse<>(null));
    }

    private boolean isTokenNotRole(String token, String role) throws JwtTokenMalformedException, JwtTokenMissingException {
        return !jwtUtil.getUserRole(token).equals(role);
    }
}
