package com.gdaditya.trainingservice.util;

import com.gdaditya.trainingservice.exception.JwtTokenMalformedException;
import com.gdaditya.trainingservice.exception.JwtTokenMissingException;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Log4j2
@Component
public class JwtUtil {

    private Key key;

    @Autowired
    public JwtUtil(@Value("${jwt.secret}") String key) {
        String keyStr = key + " ".repeat(Math.max(0, 32 - key.length()));
        this.key = Keys.hmacShaKeyFor(keyStr.getBytes(StandardCharsets.UTF_8));
    }

    public String generateApiToken() {
        Date now = new Date(System.currentTimeMillis());
        Date expiryDate = new Date(now.getTime() + 120000);

        Map<String, Object> claims = new HashMap<>();
        claims.put("role", "SERVICE");

        return Jwts.builder()
                .setId("service") // with claim, this will be replaced
                .setSubject("service") // with claim, this will be replaced
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(expiryDate)
                .signWith(key)
                .compact();
    }

    public Long getUserId(String token) throws JwtTokenMalformedException, JwtTokenMissingException {
        Claims claims = getClaims(token);
        return Long.parseLong(String.valueOf(claims.get("id")));
    }

    public String getUserRole(String token) throws JwtTokenMalformedException, JwtTokenMissingException {
        Claims claims = getClaims(token);
        return String.valueOf(claims.get("role"));
    }

    public Claims getClaims(final String token) throws JwtTokenMalformedException, JwtTokenMissingException {
        try {
            String parsedToken = token;
            if (token.startsWith("Bearer "))
                parsedToken = parsedToken.substring(7);
            Jws<Claims> claimsJws = Jwts.parser().setSigningKey(key).parseClaimsJws(parsedToken);
            return claimsJws.getBody();
        } catch (SignatureException ex) {
            throw new JwtTokenMalformedException("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            throw new JwtTokenMalformedException("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            throw new JwtTokenMalformedException("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            throw new JwtTokenMalformedException("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            throw new JwtTokenMissingException("JWT claims string is empty");
        } catch (Exception ex) {
            log.error(ex);
            throw ex;
        }
    }

}
