package com.gdaditya.trainingservice.logging;

public interface LogService {
    void log(String message);
    void logRequest(String ip, String path, String method);
}
