package com.gdaditya.trainingservice.config;

import com.gdaditya.trainingservice.model.Category;
import com.gdaditya.trainingservice.model.EnrollmentStatus;
import com.gdaditya.trainingservice.repository.CategoryRepository;
import com.gdaditya.trainingservice.repository.EnrollmentStatusRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class TrainingConfig {
    private final CategoryRepository categoryRepository;
    private final EnrollmentStatusRepository enrollmentStatusRepository;

    @Bean
    public ApplicationRunner runner() {
        return args -> {
            categoryRepository.save(new Category("Back-end"));
            categoryRepository.save(new Category("Front-end"));
            categoryRepository.save(new Category("Devops"));
            categoryRepository.save(new Category("Security"));

            enrollmentStatusRepository.save(new EnrollmentStatus("Pending"));
            enrollmentStatusRepository.save(new EnrollmentStatus("Accepted"));
        };
    }

}
