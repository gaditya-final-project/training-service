package com.gdaditya.trainingservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="academy_user")
public class User {
    @Id
    private Long id;
    private String username;
    private String email;

    @JsonIgnore
    public Map<String, Object> getJsonChildMap() {
        Map<String, Object> res = new HashMap<>();
        res.put("id", this.getId());
        res.put("username", this.getUsername());
        res.put("email", this.getEmail());
        return res;
    }
}
