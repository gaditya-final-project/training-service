package com.gdaditya.trainingservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Training {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(nullable = false, name = "trainer")
    private User trainer;

    @ManyToOne
    @JoinColumn(nullable = false, name = "training_category_id")
    private Category category;

    @OneToMany(mappedBy = "training", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<TrainingEnrollment> enrolledUsers = new ArrayList<>();

    @Column
    @Builder.Default
    private String description = "";

    @Column(name = "created_at")
    @CreationTimestamp
    protected Date createdAt;

    @Column(name = "updated_at")
    @UpdateTimestamp
    protected Date updatedAt;

    @JsonIgnore
    public Map<String, Object> getJsonChildMap() {
        Map<String, Object> res = new HashMap<>();
        res.put("id", this.getId());
        res.put("name", this.getName());
        res.put("category", this.getCategory().getJsonChildMap());
        return res;
    }
}
