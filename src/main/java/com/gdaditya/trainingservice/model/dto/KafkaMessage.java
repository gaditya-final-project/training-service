package com.gdaditya.trainingservice.model.dto;

import lombok.Data;

@Data
public class KafkaMessage {
    private Long id;
    private String type;
    private Object content;

}
