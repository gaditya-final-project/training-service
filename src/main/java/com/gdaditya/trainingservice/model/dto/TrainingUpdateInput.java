package com.gdaditya.trainingservice.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class TrainingUpdateInput {
    @NotNull(message = "Id cannot be empty!")
    private Long id;
    private String name;
    private Long categoryId;
    private String description;
}
