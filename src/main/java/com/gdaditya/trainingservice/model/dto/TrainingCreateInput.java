package com.gdaditya.trainingservice.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class TrainingCreateInput {
    @NotEmpty(message = "Name cannot be empty!")
    private String name;
    @NotNull(message = "Category ID cannot be empty!")
    private Long categoryId;
    private String description;
}
