package com.gdaditya.trainingservice.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class EnrollmentUpdateInput {
    @NotNull(message = "User ID cannot be empty!")
    Long userId;
    @NotNull(message = "Training ID cannot be empty!")
    Long trainingId;
    @NotNull(message = "Status ID cannot be empty!")
    Long statusId;
}
