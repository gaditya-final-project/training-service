package com.gdaditya.trainingservice.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class EnrollmentCreateInput {
    @NotNull(message = "Training ID cannot be empty!")
    Long trainingId;
}
