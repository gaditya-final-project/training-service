package com.gdaditya.trainingservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Category {
    private static final long serialVersionUID = 1L;

    public Category(String name) {
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(name = "created_at")
    @CreationTimestamp
    @JsonIgnore
    protected Date createdAt;

    @Column(name = "updated_at")
    @UpdateTimestamp
    @JsonIgnore
    protected Date updatedAt;

    @JsonIgnore
    public Map<String, Object> getJsonChildMap() {
        Map<String, Object> res = new HashMap<>();
        res.put("id", id);
        res.put("name", name);
        return res;
    }
}