package com.gdaditya.trainingservice.service;

import com.gdaditya.trainingservice.exception.BaseException;
import com.gdaditya.trainingservice.exception.JwtTokenMalformedException;
import com.gdaditya.trainingservice.exception.JwtTokenMissingException;
import com.gdaditya.trainingservice.model.Category;
import com.gdaditya.trainingservice.model.Training;
import com.gdaditya.trainingservice.model.dto.TrainingCreateInput;
import com.gdaditya.trainingservice.model.dto.TrainingUpdateInput;

import java.util.List;

public interface TrainingService {
    Training createTraining(TrainingCreateInput trainingCreateInput, String token) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException;
    Training getTrainingFromId(Long id) throws BaseException;
    List<Training> getAllTrainingFromCategory(Long categoryId) throws BaseException;
    List<Training> getAllCreatedTraining(String token) throws JwtTokenMalformedException, JwtTokenMissingException, BaseException;
    List<Training> getAllTrainingFromTrainer(Long userId) throws BaseException;
    List<Training> getAllTraining();
    Training updateTraining(TrainingUpdateInput trainingUpdateInput, String token) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException;
    void deleteTrainingFromId(Long id) throws BaseException;
}
