package com.gdaditya.trainingservice.service;

import com.gdaditya.trainingservice.exception.BaseException;
import com.gdaditya.trainingservice.model.UserCategoryPreferences;

import java.util.List;

public interface UserCategoryPreferencesService {
    UserCategoryPreferences createUserCategoryPreferences(Long userId, Long categoryId) throws BaseException;
    List<UserCategoryPreferences> getAllUserCategoryPreferencesFromUserId(Long userId) throws BaseException;
    List<UserCategoryPreferences> getAllUserCategoryPreferencesFromCategoryId(Long categoryId);
    void deleteUserCategoryPreferences(Long userId, Long categoryId) throws BaseException;
}
