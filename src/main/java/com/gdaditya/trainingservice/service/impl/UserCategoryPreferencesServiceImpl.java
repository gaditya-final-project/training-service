package com.gdaditya.trainingservice.service.impl;

import com.gdaditya.trainingservice.exception.BaseException;
import com.gdaditya.trainingservice.model.Category;
import com.gdaditya.trainingservice.model.User;
import com.gdaditya.trainingservice.model.UserCategoryPreferences;
import com.gdaditya.trainingservice.repository.CategoryRepository;
import com.gdaditya.trainingservice.repository.UserCategoryPreferencesRepository;
import com.gdaditya.trainingservice.service.UserCategoryPreferencesService;
import com.gdaditya.trainingservice.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserCategoryPreferencesServiceImpl implements UserCategoryPreferencesService {

    private final UserService userService;
    private final CategoryRepository categoryRepository;
    private final UserCategoryPreferencesRepository userCategoryPreferencesRepository;

    @Override
    public UserCategoryPreferences createUserCategoryPreferences(Long userId, Long categoryId) throws BaseException {
        User user = userService.getUserFromId(userId);
        Category category = categoryRepository.findCategoryById(categoryId);
        UserCategoryPreferences userCategoryPreferences = userCategoryPreferencesRepository.findUserCategoryPreferencesByUserAndCategory(user, category);
        if (userCategoryPreferences == null)
            userCategoryPreferences = new UserCategoryPreferences(user, category);
        return userCategoryPreferencesRepository.save(userCategoryPreferences);
    }

    @Override
    public List<UserCategoryPreferences> getAllUserCategoryPreferencesFromUserId(Long userId) throws BaseException {
        User user = userService.getUserFromId(userId);
        return userCategoryPreferencesRepository.findAllByUser(user);
    }

    @Override
    public List<UserCategoryPreferences> getAllUserCategoryPreferencesFromCategoryId(Long categoryId) {
        Category category = categoryRepository.findCategoryById(categoryId);
        return userCategoryPreferencesRepository.findAllByCategory(category);
    }

    @Override
    public void deleteUserCategoryPreferences(Long userId, Long categoryId) throws BaseException {
        User user = userService.getUserFromId(userId);
        Category category = categoryRepository.findCategoryById(categoryId);
        UserCategoryPreferences userCategoryPreferences = userCategoryPreferencesRepository.findUserCategoryPreferencesByUserAndCategory(user, category);
        userCategoryPreferencesRepository.delete(userCategoryPreferences);
    }
}
