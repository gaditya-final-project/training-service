package com.gdaditya.trainingservice.service.impl;

import com.gdaditya.trainingservice.exception.BaseException;
import com.gdaditya.trainingservice.exception.JwtTokenMalformedException;
import com.gdaditya.trainingservice.exception.JwtTokenMissingException;
import com.gdaditya.trainingservice.exception.UnauthorizedException;
import com.gdaditya.trainingservice.model.*;
import com.gdaditya.trainingservice.model.dto.EnrollmentCreateInput;
import com.gdaditya.trainingservice.model.dto.EnrollmentUpdateInput;
import com.gdaditya.trainingservice.repository.EnrollmentStatusRepository;
import com.gdaditya.trainingservice.repository.TrainingEnrollmentRepository;
import com.gdaditya.trainingservice.service.EnrollmentService;
import com.gdaditya.trainingservice.service.TrainingService;
import com.gdaditya.trainingservice.service.UserService;
import com.gdaditya.trainingservice.util.JwtUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class EnrollmentServiceImpl implements EnrollmentService {

    private final TrainingService trainingService;
    private final UserService userService;
    private final EnrollmentStatusRepository enrollmentStatusRepository;
    private final TrainingEnrollmentRepository enrollmentRepository;
    private final JwtUtil jwtUtil;

    @Override
    public TrainingEnrollment createEnrollment(EnrollmentCreateInput enrollmentUpdateInput, String token) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        Training training = trainingService.getTrainingFromId(enrollmentUpdateInput.getTrainingId());
        EnrollmentStatus enrollmentStatus = enrollmentStatusRepository.findEnrollmentStatusById((long)1);
        Long id = jwtUtil.getUserId(token);
        User user = userService.getUserFromId(id);
        TrainingEnrollment trainingEnrollment1 = enrollmentRepository.findTrainingEnrollmentByUserAndTraining(user, training);
        if (trainingEnrollment1 != null)
            throw new BaseException(HttpStatus.BAD_REQUEST, "Already enrolled to requested training!");

        TrainingEnrollment trainingEnrollment = TrainingEnrollment.builder()
                .training(training)
                .user(user)
                .status(enrollmentStatus)
                .build();

        return enrollmentRepository.save(trainingEnrollment);
    }

    @Override
    public TrainingEnrollment getEnrollment(Long userId, Long trainingId, String token) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        String role = jwtUtil.getUserRole(token);
        Long id = jwtUtil.getUserId(token);
        if (!Objects.equals(role, "ADMIN") && !trainingId.equals(id))
            throw new UnauthorizedException();

        Training training = trainingService.getTrainingFromId(trainingId);
        User user = userService.getUserFromId(userId);
        TrainingEnrollment trainingEnrollment = enrollmentRepository.findTrainingEnrollmentByUserAndTraining(user, training);
        if (trainingEnrollment == null)
            throw new BaseException(HttpStatus.NOT_FOUND, "Enrollment not found!");
        return trainingEnrollment;
    }

    @Override
    public List<TrainingEnrollment> getEnrollmentFromUserId(Long userId, String token) throws JwtTokenMalformedException, JwtTokenMissingException, BaseException {
        String role = jwtUtil.getUserRole(token);
        Long id = jwtUtil.getUserId(token);
        if (!Objects.equals(role, "ADMIN") && !userId.equals(id))
            throw new UnauthorizedException();

        User user = userService.getUserFromId(userId);
        return enrollmentRepository.findAllByUser(user);
    }

    @Override
    public List<TrainingEnrollment> getEnrollmentFromTrainingId(Long trainingId, String token) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        Training training = trainingService.getTrainingFromId(trainingId);

        String role = jwtUtil.getUserRole(token);
        Long id = jwtUtil.getUserId(token);
        if (!Objects.equals(role, "ADMIN") && !training.getTrainer().getId().equals(id))
            throw new UnauthorizedException();

        return enrollmentRepository.findAllByTraining(training);
    }

    @Override
    public List<TrainingEnrollment> getAllEnrollment() {
        return enrollmentRepository.findAll();
    }

    @Override
    public TrainingEnrollment updateEnrollment(EnrollmentUpdateInput enrollmentCreateInput) throws BaseException {
        Training training = trainingService.getTrainingFromId(enrollmentCreateInput.getTrainingId());
        User user = userService.getUserFromId(enrollmentCreateInput.getUserId());
        EnrollmentStatus enrollmentStatus = enrollmentStatusRepository.findEnrollmentStatusById(enrollmentCreateInput.getStatusId());
        if (enrollmentStatus == null)
            throw new BaseException(HttpStatus.BAD_REQUEST, "Enrollment status with id " + enrollmentCreateInput.getStatusId() + " does not exist!");

        TrainingEnrollment trainingEnrollment = enrollmentRepository.findTrainingEnrollmentByUserAndTraining(user, training);
        if (trainingEnrollment == null)
            throw new BaseException(HttpStatus.NOT_FOUND, "Enrollment not found!");
        trainingEnrollment.setStatus(enrollmentStatus);
        return enrollmentRepository.save(trainingEnrollment);
    }

    @Override
    @Transactional
    public void deleteEnrollment(Long userId, Long trainingId) throws BaseException {
        Training training = trainingService.getTrainingFromId(trainingId);
        User user = userService.getUserFromId(userId);

        TrainingEnrollment trainingEnrollment = enrollmentRepository.findTrainingEnrollmentByUserAndTraining(user, training);
        if (trainingEnrollment == null)
            throw new BaseException(HttpStatus.NOT_FOUND, "Enrollment not found!");

        enrollmentRepository.delete(trainingEnrollment);
    }
}
