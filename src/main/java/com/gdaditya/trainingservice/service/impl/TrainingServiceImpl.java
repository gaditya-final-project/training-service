package com.gdaditya.trainingservice.service.impl;

import com.gdaditya.trainingservice.exception.BaseException;
import com.gdaditya.trainingservice.exception.JwtTokenMalformedException;
import com.gdaditya.trainingservice.exception.JwtTokenMissingException;
import com.gdaditya.trainingservice.exception.UnauthorizedException;
import com.gdaditya.trainingservice.model.Category;
import com.gdaditya.trainingservice.model.Training;
import com.gdaditya.trainingservice.model.User;
import com.gdaditya.trainingservice.model.dto.TrainingCreateInput;
import com.gdaditya.trainingservice.model.dto.TrainingUpdateInput;
import com.gdaditya.trainingservice.repository.CategoryRepository;
import com.gdaditya.trainingservice.repository.TrainingRepository;
import com.gdaditya.trainingservice.service.TrainingService;
import com.gdaditya.trainingservice.service.UserService;
import com.gdaditya.trainingservice.util.JwtUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class TrainingServiceImpl implements TrainingService {

    private final UserService userService;
    private final CategoryRepository categoryRepository;
    private final TrainingRepository trainingRepository;
    private final JwtUtil jwtUtil;

    @Override
    public Training createTraining(TrainingCreateInput trainingCreateInput, String token) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        Category category = categoryRepository.findCategoryById(trainingCreateInput.getCategoryId());
        if (category == null)
            throw new BaseException(HttpStatus.BAD_REQUEST, "Category with id " + trainingCreateInput.getCategoryId() + " does not exist!");
        Long userId = jwtUtil.getUserId(token);
        User user = userService.getUserFromId(userId);
        Training training = Training.builder()
                .name(trainingCreateInput.getName())
                .category(category)
                .trainer(user)
                .description(trainingCreateInput.getDescription())
                .build();

        return trainingRepository.save(training);
    }

    @Override
    public Training getTrainingFromId(Long id) throws BaseException {
        Training training = trainingRepository.findTrainingById(id);
        if (training == null)
            throw new BaseException(HttpStatus.NOT_FOUND, "Training not found!");
        return training;
    }

    @Override
    public List<Training> getAllTrainingFromCategory(Long categoryId) throws BaseException {
        Category category = categoryRepository.findCategoryById(categoryId);
        if (category == null)
            throw new BaseException(HttpStatus.BAD_REQUEST, "Category with id " + categoryId + " does not exist!");
        return trainingRepository.findAllByCategory(category);
    }

    @Override
    public List<Training> getAllCreatedTraining(String token) throws JwtTokenMalformedException, JwtTokenMissingException, BaseException {
        Long id = jwtUtil.getUserId(token);
        User user = userService.getUserFromId(id);
        if (user == null)
            throw new BaseException(HttpStatus.BAD_REQUEST, "Trainer user with id " + id + " does not exist!");
        return trainingRepository.findAllByTrainer(user);
    }

    @Override
    public List<Training> getAllTrainingFromTrainer(Long userId) throws BaseException {
        User user = userService.getUserFromId(userId);
        if (user == null)
            throw new BaseException(HttpStatus.BAD_REQUEST, "Trainer user with id " + userId + " does not exist!");
        return trainingRepository.findAllByTrainer(user);
    }

    @Override
    public List<Training> getAllTraining() {
        return trainingRepository.findAll();
    }

    @Override
    public Training updateTraining(TrainingUpdateInput trainingUpdateInput, String token) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        Long trainingId =  trainingUpdateInput.getId();
        Training training = trainingRepository.findTrainingById(trainingId);
        if (training == null)
            throw new BaseException(HttpStatus.BAD_REQUEST, "Training with id " + trainingId + " does not exist!");

        String role = jwtUtil.getUserRole(token);
        Long id = jwtUtil.getUserId(token);
        if (!Objects.equals(role, "ADMIN") && !training.getTrainer().getId().equals(id))
            throw new UnauthorizedException();

        if (trainingUpdateInput.getName() != null)
            training.setName(trainingUpdateInput.getName());
        if (trainingUpdateInput.getCategoryId() != null) {
            Category category = categoryRepository.findCategoryById(trainingUpdateInput.getCategoryId());
            if (category == null)
                throw new BaseException(HttpStatus.BAD_REQUEST, "Category with id " + trainingUpdateInput.getCategoryId() + " does not exist!");
            training.setCategory(category);
        } if (trainingUpdateInput.getDescription() != null)
            training.setDescription(trainingUpdateInput.getDescription());
        return trainingRepository.save(training);
    }

    @Override
    @Transactional
    public void deleteTrainingFromId(Long id) throws BaseException {
        Training training = trainingRepository.findTrainingById(id);
        if (training == null)
            throw new BaseException(HttpStatus.NOT_FOUND, "Training not found!");
        trainingRepository.delete(training);
    }
}
