package com.gdaditya.trainingservice.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gdaditya.trainingservice.exception.BaseException;
import com.gdaditya.trainingservice.model.User;
import com.gdaditya.trainingservice.model.dto.BaseResponse;
import com.gdaditya.trainingservice.repository.UserRepository;
import com.gdaditya.trainingservice.service.UserService;
import com.gdaditya.trainingservice.util.JwtUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@Log4j2
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RestTemplate restTemplate;
    private final JwtUtil jwtUtil;
    private final ObjectMapper mapper;
    private static final String USER_ADDR = "http://auth-service/api/user/";

    @Override
    public User getUserFromId(Long id) throws BaseException {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(jwtUtil.generateApiToken());
        HttpEntity<String> entity = new HttpEntity<>("body", headers);
        ResponseEntity<BaseResponse> resp = null;
        try {
            resp = restTemplate.exchange(USER_ADDR+id, HttpMethod.GET, entity, BaseResponse.class);
        } catch (Exception e) {
            if (e instanceof HttpClientErrorException.NotFound)
                throw new BaseException(HttpStatus.BAD_REQUEST, "User not found!");
            log.warn(e);
        }
        if (resp != null && resp.getStatusCode() == HttpStatus.OK && resp.getBody() != null) {
            User user = mapper.convertValue(resp.getBody().getData(), User.class);
            return userRepository.save(user);
        } else
            throw new BaseException(HttpStatus.INTERNAL_SERVER_ERROR, "Error communicating to user service.");
    }
}
