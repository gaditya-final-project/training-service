package com.gdaditya.trainingservice.service;

import com.gdaditya.trainingservice.exception.BaseException;
import com.gdaditya.trainingservice.model.User;

public interface UserService {
    User getUserFromId(Long id) throws BaseException;
}
