package com.gdaditya.trainingservice.service;

import com.gdaditya.trainingservice.exception.BaseException;
import com.gdaditya.trainingservice.exception.JwtTokenMalformedException;
import com.gdaditya.trainingservice.exception.JwtTokenMissingException;
import com.gdaditya.trainingservice.model.TrainingEnrollment;
import com.gdaditya.trainingservice.model.dto.EnrollmentCreateInput;
import com.gdaditya.trainingservice.model.dto.EnrollmentUpdateInput;

import java.util.List;

public interface EnrollmentService {
    TrainingEnrollment createEnrollment(EnrollmentCreateInput enrollmentCreateInput, String token) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException;
    TrainingEnrollment getEnrollment(Long userId, Long trainingId, String token) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException;
    List<TrainingEnrollment> getEnrollmentFromUserId(Long id, String token) throws JwtTokenMalformedException, JwtTokenMissingException, BaseException;
    List<TrainingEnrollment> getEnrollmentFromTrainingId(Long id, String token) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException;
    List<TrainingEnrollment> getAllEnrollment();
    TrainingEnrollment updateEnrollment(EnrollmentUpdateInput enrollmentCreateInput) throws BaseException;
    void deleteEnrollment(Long userId, Long trainingId) throws BaseException;
}
