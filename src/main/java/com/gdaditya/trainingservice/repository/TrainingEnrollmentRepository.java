package com.gdaditya.trainingservice.repository;

import com.gdaditya.trainingservice.model.Training;
import com.gdaditya.trainingservice.model.TrainingEnrollment;
import com.gdaditya.trainingservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrainingEnrollmentRepository extends JpaRepository<TrainingEnrollment, Long> {
    TrainingEnrollment findTrainingEnrollmentByUserAndTraining(User user, Training training);
    List<TrainingEnrollment> findAllByUser(User user);
    List<TrainingEnrollment> findAllByTraining(Training training);
}
