package com.gdaditya.trainingservice.repository;

import com.gdaditya.trainingservice.model.Category;
import com.gdaditya.trainingservice.model.User;
import com.gdaditya.trainingservice.model.UserCategoryPreferences;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserCategoryPreferencesRepository extends JpaRepository<UserCategoryPreferences, Long> {
    List<UserCategoryPreferences> findAllByUser(User user);
    List<UserCategoryPreferences> findAllByCategory(Category category);
    UserCategoryPreferences findUserCategoryPreferencesByUserAndCategory(User user, Category category);
}
