package com.gdaditya.trainingservice.repository;

import com.gdaditya.trainingservice.model.EnrollmentStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnrollmentStatusRepository extends JpaRepository<EnrollmentStatus, Long> {
    EnrollmentStatus findEnrollmentStatusById(Long id);
    EnrollmentStatus findEnrollmentStatusByName(String name);
}
