package com.gdaditya.trainingservice.repository;

import com.gdaditya.trainingservice.model.Category;
import com.gdaditya.trainingservice.model.Training;
import com.gdaditya.trainingservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrainingRepository extends JpaRepository<Training, Long> {
    Training findTrainingById(Long id);
    List<Training> findAllByCategory(Category category);
    List<Training> findAllByTrainer(User trainer);
}
